//Seren
//22/03
//10010681

//Declaring what the variables are
let height
let bodyMassIndex

//Determing the value of the variables. Weight and Height are determined by the user where they are 
//"(prompt)" to enter a "(Number)" from the question ("Enter the person's weight(kg):") & ("Enter the
//person's height(cm):")/100 which is divided by 100 (/100). bodyMassIndex variable is determined by
//an equation using the height & weight. It reads BMI = (weight/(height*height) ".toFixed(1) means the
//number will be set to ("1") decimal place. Category is determined by looking at an "if" statement for
//bodyMassIndex.
weight = Number(prompt("Enter the person's weight(kg):"))
height = (Number(prompt("Enter the person's height(cm):"))/100)

bodyMassIndex = (weight/(height*height).toFixed(1))

console.log("Body Mass Index Calculator")//Displays title
console.log("")//Used to miss a line ("typing with no text")

console.log(`Person's weight : ${weight} kg`)//Displays person's weight
console.log(`Person's height : ${height} cm`)//Displays person's height
console.log("")//Used to miss a line ("typing with no text")
console.log(`Body Mass Index : ${bodyMassIndex.toFixed(1)}`)//Displays person's BMI