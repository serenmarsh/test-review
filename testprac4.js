//Seren
//22/03
//10010681

let monthRain
let totalRain = 0

console.log("Rainfall (mm) for the year");

for (i = 1; i<=12; i++){
    monthRain = Number(prompt(`Enter the rainfall (mm) for month ${i}`))
    console.log(`Rainfall for month ${i} : ${monthRain}`) 
    totalRain = Number(totalRain + monthRain)
}

console.log("")
console.log(`Total Rainfall : ${totalRain.toFixed(1)}`)

