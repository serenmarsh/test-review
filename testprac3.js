//Seren
//22/03
//10010681

//Declaring what the variables are
let value;
let rounding;

//Determing the value of the variables. Value is determined by the user where they are 
//"(prompt)" to enter a "(Number)" from the question ("Enter the last cent value...")
value = (Number(prompt("Enter the last cent value of the transaction (1 to 9):"))

//switch statement
//For whatever number the user inputs (1-9) the switch statement will keep checking until it
//matches a case then it will execute that case's code. For example if the user input "7" for the value
//the switch statement will match case 7 resulting in the rounding to be determined as "up"
//the break declares a new case. You can have several amounts of cases. The break is used to declare
//a new case. default is used if no cases have been met.
switch(value) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
        rounding = "down"
        break
    case 6:
    case 7:
    case 8:
    case 9:
        rounding = "up"
        break
    default:
        rounding = "unknown"
}

//Display results as showing the rounding "value" in cents and what "rounding" type to apply
console.log("Determine rounding type for a cash transaction");
console.log("");

console.log(`Cash transaction ends with : ${value} cents`)
console.log(`Rounding type to apply : ${rounding}`)